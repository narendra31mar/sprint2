package com.ata.calculator;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class ATACalcController {
	
	@Autowired
	AtaDAO reg;
	
	// To view Login Page
		@RequestMapping(value = "sprint1/atacalc", method = RequestMethod.GET)
		public ModelAndView sprint1(Locale locale, Model model, ATABean lb) {
			ModelAndView web = new ModelAndView("sprint1/atacalc", "command", new ATABean());
			web.setViewName("sprint1/atacalc");
			return web;
		}
		
}
