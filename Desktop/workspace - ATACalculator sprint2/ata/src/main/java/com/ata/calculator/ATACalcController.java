package com.ata.calculator;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class ATACalcController {
	
	@Autowired
	AtaDAO reg;
	
		
		@RequestMapping(value = "sprint2/atacalc", method = RequestMethod.GET)
		public ModelAndView sprint2(Locale locale, Model model, ATABean lb) {
			ModelAndView web = new ModelAndView("sprint2/atacalc", "command", new ATABean());
			web.setViewName("sprint2/atacalc");
			return web;
		}


}
