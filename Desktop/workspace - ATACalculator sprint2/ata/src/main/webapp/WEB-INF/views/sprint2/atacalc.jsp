<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
	<head>
		<title>ATA Super Smart Calculator</title>
		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/mytheme/css/style.css"/>">
		<script src="<c:url value="/resources/mytheme/jquery/jquery-1.10.2.js"/>"></script>
		<script type="text/javascript">
			function myFunction()
				{
				var x = document.getElementById("first").value;
				var y = document.getElementById("second").value;
				var z = document.getElementById("result");

				if (document.getElementById('r1').checked)
						 {
						z.value = parseInt(x) + parseInt(y) ;						
						 }
				else if (document.getElementById('r2').checked)
						{
						z.value = parseInt(x) * parseInt(y) ;
					   	}
				else if (document.getElementById('r3').checked)
						{
						z.value = parseInt(x) * parseInt(x) ;
					   	}
				else if (document.getElementById('r4').checked)
						{
						if(parseInt(x) > parseInt(y))
							{
							z.value = parseInt(x);
					   		}
						else if(parseInt(y) > parseInt(x))
							{
							z.value = parseInt(y);
							}
						}
				else if (document.getElementById('r5').checked)
						{
						z.value = parseInt(x) * parseInt(x) - parseInt(y) * parseInt(y);
					   	}
				else if (document.getElementById('r6').checked)
					{
						z.value = parseInt(x) * parseInt(x) + 2 * parseInt(x) * parseInt(y) + parseInt(y) * parseInt(y);
				   	}
				else if (document.getElementById('r7').checked)
					{
					z.value = parseInt(x) * parseInt(x) - 2 * parseInt(x) * parseInt(y) + parseInt(y) * parseInt(y);
				   	}
				}		
		</script>
		
	</head>
	<body>
		<div class="container">
			<div class="logo">
				 <img src="<c:url value="/resources/mytheme/images/ATA_Logo_Trans.png"/>">
			</div>
			<h1>ATA Super Smart Calculator</h1>
			<form name="mainForm" id="mainForm">
				<div class="field_content">
					<label>Please enter first number:</label>
					<input type="text" id="first" size="3" maxlength="3" 
							onKeyPress="return numbersonly(this, event)">
				</div>
				<div class="field_content">
					<label>Please enter second number:</label>
					<input type="text" id="second" size="3" maxlength="3" 
							onKeyPress="return numbersonly(this, event)">
				</div>
				<div class="radio_content">
					<div class="radio_check">
						<input type="radio" id="r1" name="radio" value="add">
						<span>Add</span>
					</div>
					<div class="radio_check">
						<input type="radio" id="r2" name="radio" value="mul">
						<span>Mul</span>
					</div>
					<div class="radio_check">
						<input type="radio" id="r3" name="radio" value="sqr">
						<span>Sqr</span>
					</div>
					<div class="radio_check">
						<input type="radio" id="r4" name="radio" value="comp">
						<span>Comp</span>
					</div>
					<div class="radio_check">
						<input type="radio" id="r5" name="radio" value="sqrsub">
						<span>SqrSub</span>
					</div>
					<div class="radio_check">
						<input type="radio" id="r6" name="radio" value="eucidplus">
						<span>Euclid(+)</span>
					</div>
					<div class="radio_check">
						<input type="radio" id="r7" name="radio" value="eucidminus">
						<span>Euclid(-)</span>
					</div>
				</div>
				<div class="cal_btn">
					<input type="button" id="Calculate" value="Calculate" onclick="myFunction()">
				</div>
				<div class="field_content">
					<label>Result(Value):</label>
					<input type="text" id="result" disabled>
				</div>
			</form>
		</div>
	</body>
	
	<script type="text/javascript">
		$(document).ready(function() {
			$('#Calculate').click(function() {
			    if($("#first").val().length == 0) {
			        alert("Please enter first number");
			    }
			    if(parseInt($("#first").val()) > 999) {
			    	alert("Number should not be more than 1000");
			    }
			    
			    if($("#second").val().length == 0) {
			        alert("Please enter second number:");
			    }
			    if(parseInt($("#second").val()) > 999) {
			    	alert("Number should not be more than 1000");
			    }
			    
			    checked = $("input[type=radio]:checked").length;
				if(!checked) {
				  alert("You select equation option");
				  return false;
				}
			    
			});
		});

		// Number only in input field.
		function numbersonly(myfield, e, dec)
		{
		var key;
		var keychar;
	
		if (window.event)
		   key = window.event.keyCode;
		else if (e)
		   key = e.which;
		else
		   return true;
		keychar = String.fromCharCode(key);
	
		// control keys
		if ((key==null) || (key==0) || (key==8) || 
		    (key==9) || (key==13) || (key==27) )
		   return true;
	
		// numbers
		else if ((("0123456789").indexOf(keychar) > -1))
		   return true;
	
		// decimal point jump
		else if (dec && (keychar == "."))
		   {
		   myfield.form.elements[dec].focus();
		   return false;
		   }
		else
		   return false;
		}
	</script>
</html>